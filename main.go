package main

import (
	"fmt"
	"os"
	"./cars"
)

func main() {
	list := cars.Get()
	if len(list) == 0 { // If an error occurred, don't write
		return
	}

	f, err := os.Create("cars.csv")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer f.Close()

	fmt.Fprintln(f, "Name,Mileage,Price,Status,Date,Link,Description")
	for _, car := range list {
		if car.Mileage() != 0 {
			fmt.Fprintf(f, "\"%s\",%d,$%d,%s,%s,\"%s\",\"%s\"\n", car.Name, car.Mileage(), car.Price, car.Status, car.Date, car.Link, car.About)
		} else {
			fmt.Fprintf(f, "\"%s\",,$%d,%s,%s,\"%s\",\"%s\"\n", car.Name, car.Price, car.Status, car.Date, car.Link, car.About)
		}
	}
}
