package cars

import (
	"strings"
	"strconv"
	"github.com/PuerkitoBio/goquery"
	"regexp"
)

type Car struct {
	Name string	
	About string
	Price uint64 // 0 if no price found
	Link string
	Status string
	Date string
}

func (c* Car) String() string {
	if c.Price == 0 {
		return c.Name
	} else {
		return c.Name + " for $" + strconv.FormatUint(c.Price, 10)
	}
}

func (c* Car) Mileage() uint64 {
	regex, _ := regexp.Compile("[,0-9]+k?\\-[Mm]ile")
	mileage := regex.FindAllString(c.Name, 1)

	// If nothing in title, try excerpt
	if mileage == nil {
		regex, _ = regexp.Compile("[,0-9]+k?\\s[Mm]ile")
		mileage = regex.FindAllString(c.About, 1)
	}
	if mileage != nil {
		mileage := strings.NewReplacer("k", "000", ",", "").Replace(mileage[0])
		ret, err := strconv.ParseUint(mileage[0:len(mileage) - 5], 10, 64)
		if err == nil {
			return ret
		}
	}
	return 0
}

func unsoldNodeToCar(node *goquery.Selection) (*Car) {
	out := new(Car)
	out.Name = node.Find(".auctions-item-title").First().Text()
	out.Link = node.Find(".auctions-item-title").Find("a").AttrOr("href", "")
	out.About = strings.Trim(node.Find(".auctions-item-excerpt").First().Text(), "\n ")
	priceStr := node.Find(".auctions-item-meta-value:contains(\"$\")").First().Text()
	price, err := strconv.ParseUint(strings.NewReplacer("$", "", ",", "").Replace(priceStr), 10, 64)
	if err == nil {
		out.Price = price
	}
	out.Status = "bidding"
	return out
}

func soldNodeToCar(node *goquery.Selection) (*Car) {
	out := new(Car)
	out.Name = node.Find(".auctions-item-title").First().Text()
	out.Link = node.Find(".auctions-item-title").Find("a").AttrOr("href", "")
	out.About = strings.Trim(node.Find(".auctions-item-status").First().Text(), "\n ")
	i := strings.Index(out.About, "$")
	if i != -1 {
		priceStr := out.About[i+1:]
		price, err := strconv.ParseUint(strings.NewReplacer("$", "", ",", "", " ", "").Replace(priceStr), 10, 64)
		if err == nil {
			out.Price = price
		}
	}

	if(strings.Index(out.About, "Reserve") != -1) {
		out.Status = "reserve"
	} else if(strings.Index(out.About, "Sold") != -1) {
		out.Status = "sold"
	}

	regex, _ := regexp.Compile("[0-9]+/[0-9]+/[0-9]+")
	date := regex.FindAllString(out.About, 1)
	if date != nil {
		out.Date = date[0]
	}

	return out
}

func Get() []*Car {
	var out []*Car

	doc, err := goquery.NewDocument("https://bringatrailer.com/auctions")
	if err != nil {
		return nil
	}
	currentAuctions := doc.Find(".auctions-current").Not(".auctions-current-sorter").Find(".auctions-item")
	currentAuctions.Each(func(i int, s *goquery.Selection) {
		car := unsoldNodeToCar(s)
		out = append(out, car)
	})

	doc, err = goquery.NewDocument("https://bringatrailer.com/auctions/results")
	if err != nil {
		return out
	}

	previousAuctions := doc.Find(".auctions-previous").Find(".auctions-item")
	previousAuctions.Each(func(i int, s *goquery.Selection) {
		car := soldNodeToCar(s)
		out = append(out, car)
	})

	return out
}
